{-# OPTIONS_GHC -Wall #-}
module Week2.HW02 where

-- Mastermind -----------------------------------------

-- A peg can be one of six colors
data Peg = Red | Green | Blue | Yellow | Orange | Purple
         deriving (Show, Eq, Ord)

-- A code is defined to simply be a list of Pegs
type Code = [Peg]

-- A move is constructed using a Code and two integers; the number of
-- exact matches and the number of regular matches
data Move = Move Code Int Int
          deriving (Show, Eq)

-- List containing all of the different Pegs
colors :: [Peg]
colors = [Red, Green, Blue, Yellow, Orange, Purple]

-- Exercise 1 -----------------------------------------

-- Get the number of exact matches between the actual code and the guess
exactMatches :: Code -> Code -> Int
exactMatches xs ys = length $ filter id $ zipWith (==) xs ys

-- Exercise 2 -----------------------------------------

-- Return number of times x appears in xs
occurrences :: Eq a => a -> [a] -> Int
occurrences x xs = length $ filter (== x) xs

-- For each peg in code, count how many times it occurs in colors list
countColors :: Code -> [Int]
countColors code = foldr (\c acc -> occurrences c code : acc) [] colors

-- Count number of matches between the actual code and the guess
matches :: Code -> Code -> Int
matches code guess = m (countColors code) (countColors guess) 0 where
  m [] _ acc = acc
  m _ [] acc = acc
  m (x:xs) (y:ys) acc = m xs ys (acc + min x y)

-- Exercise 3 -----------------------------------------

-- Construct a Move from a guess given the actual code
getMove :: Code -> Code -> Move
getMove code guess = let exact = exactMatches code guess in
  Move guess exact (matches code guess - exact)

-- Exercise 4 -----------------------------------------

isConsistent :: Move -> Code -> Bool
isConsistent (Move guess e n) code = let (Move _ e2 n2) = getMove code guess in
  e == e2 && n == n2

-- Exercise 5 -----------------------------------------

filterCodes :: Move -> [Code] -> [Code]
filterCodes = filter . isConsistent

-- Exercise 6 -----------------------------------------

allCodes :: Int -> [Code]
allCodes n = last $ take n $ iterate (concatMap add1) (add1 [])


add1 :: Code -> [Code]
add1 code = foldr (\v t -> (v : code) : t) [] colors

-- Exercise 7 -----------------------------------------

solve :: Code -> [Move]
solve secret = guess (replicate len Red) initial [] where
  len = length secret
  initial = allCodes len
  guess g codes moves
    | g == secret = move : moves
    | otherwise = guess (head remaining) (tail remaining) (move : moves) where
      move = getMove secret g
      remaining = filterCodes move codes


-- Bonus ----------------------------------------------

fiveGuess :: Code -> [Move]
fiveGuess = undefined
